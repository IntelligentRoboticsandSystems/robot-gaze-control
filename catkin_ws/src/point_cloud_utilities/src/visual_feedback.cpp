/******************************************************************************
 * 
 * Author: Jeremy Webb
 * Date: August 3, 2015
 * File: visual_feedback.cpp
 * Description: A ROS node to present feedback to the user on screen
 * 
 *****************************************************************************/

#include <string>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "geometry_msgs/Point32.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "pcl_ros/point_cloud.h"
#include <pcl/point_types.h>
#include <pcl/visualization/cloud_viewer.h>
#include "point_cloud_utilities/FindSubcloud.h"

class VisualFeedback{
	public:
		static const std::string IMAGE_TOPIC;
		VisualFeedback(std::string windowName = "Display");
		~VisualFeedback();
		void run();
	private:
		ros::NodeHandle nh;
		image_transport::Subscriber imageSubscriber;
		ros::Publisher clickPublisher;
ros::Publisher pcPt;
		ros::ServiceClient findObject;
		std::string windowName;
		unsigned int pixelsX, pixelsY;
		short keyPressed;

		void displayImage(const sensor_msgs::ImageConstPtr& image);

	friend void mouseClickHandler(int event, int x, int y, int flags, void* userdata);

};

void mouseClickHandler(int event, int x, int y, int flags, void* userdata){
	if(event == cv::EVENT_LBUTTONDOWN){
		VisualFeedback* feedback = static_cast<VisualFeedback*>(userdata);
		geometry_msgs::Point32 msg;
		msg.x = x;
		msg.y = y;
		feedback->clickPublisher.publish(msg);
		point_cloud_utilities::FindSubcloud sub_msg;
		sub_msg.request.index = y*640+x;
ROS_INFO("Sending clicked pt now %u", sub_msg.request.index);
		if(!feedback->findObject.call(sub_msg)){
			ROS_ERROR("subcloud message error");
		}
		else {
ROS_INFO("Received response");
			pcl::visualization::CloudViewer viewer("Cluster viewer");
			pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
			pcl::fromROSMsg(sub_msg.response.subcloud, *cloud);
		feedback->pcPt.publish(sub_msg.response.subcloud);
			viewer.showCloud(cloud);
			while(!viewer.wasStopped());
				//ros::Duration(0.01).sleep();
			ROS_INFO("viewer stopped");
		}
	}
}

const std::string VisualFeedback::IMAGE_TOPIC = "/camera/rgb/image_color";

VisualFeedback::VisualFeedback(std::string windowName)
	: windowName(windowName) {
	image_transport::ImageTransport it(nh);
	imageSubscriber = it.subscribe(IMAGE_TOPIC, 1, &VisualFeedback::displayImage, this);
	clickPublisher = nh.advertise<geometry_msgs::Point32>("click_point", 1);
	cv::namedWindow(windowName, CV_WINDOW_NORMAL | CV_WINDOW_FREERATIO);
	//cv::setWindowProperty(windowName, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	// initialize keyPressed
	keyPressed = -1;
	cv::setMouseCallback(windowName, mouseClickHandler, this);
	cv::startWindowThread();
	pixelsX = 640;
	pixelsY = 480;
	pcPt = nh.advertise<pcl::PointCloud<pcl::PointXYZRGB> >("test_point", 1);
	findObject = nh.serviceClient<point_cloud_utilities::FindSubcloud>("/cloud_utilities_node/get_subcloud");
}

VisualFeedback::~VisualFeedback(){
	cv::destroyWindow(windowName);
}

void VisualFeedback::run(){
	while(nh.ok() && keyPressed != 27){
		ros::spinOnce();
	}
}

void VisualFeedback::displayImage(const sensor_msgs::ImageConstPtr& image){
	try{
		cv::Mat newFrame = cv_bridge::toCvShare(image)->image;
		cv::Mat resizedFrame;
		cv::resize(newFrame, resizedFrame, cv::Size(pixelsX, pixelsY));
		cv::imshow(windowName, resizedFrame);
		keyPressed = cv::waitKey(40) & 255;
	}
	catch(cv_bridge::Exception& e){
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", image->encoding.c_str());
	}
}

int main(int argc, char **argv){
	ROS_INFO("Initializing visual_feedback_node");
	ros::init(argc, argv, "visual_feedback_node");
	VisualFeedback display;
	display.run();
	return 0;
}
