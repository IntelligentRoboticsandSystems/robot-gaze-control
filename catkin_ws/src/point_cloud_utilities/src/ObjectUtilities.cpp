/******************************************************************************
 * 
 * Author: Jeremy Webb
 * Date: January 16, 2016
 * File: ObjectUtilities.cpp
 * Description: A class for determining object properties, localizing objects, and classifying objects using point clouds and images
 * 
 *****************************************************************************/

#include <string>
#include <vector>

#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/PointCloud2.h>
#include "point_cloud_utilities/FindSubcloud.h"

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>

namespace objectutils{
	class ObjectUtil{
		public:
			// pcl point type used for this class
			typedef pcl::PointXYZRGB PointT;

			// topic to subscribe to to get point cloud
			static const std::string CLOUD_TOPIC;

			// Default constructor
			// nodeName can be passed in
			ObjectUtil(const std::string nodeName = "cloud_utilities_node");
	
			// service callback function for getsubcloud
			// returns the subcloud at the point specified in the request
			// if there is no subcloud at that location, this function returns false
			bool getSubcloud(point_cloud_utilities::FindSubcloud::Request &req, point_cloud_utilities::FindSubcloud::Response &res);
			bool isObject();
			// returns the next cloud from a depth topic
			pcl::PointCloud<PointT>::ConstPtr getCloud();
		private:
			// returns a pointer to the subcloud that contains pointIndex
			pcl::PointCloud<PointT>::Ptr findSubcloud(unsigned int pointIndex);
			// takes a geometry_msgs::PointStamped and converts it to a PointT
			PointT convertToPCLPt(geometry_msgs::PointStamped);
			// a callback used to ensure the connection to the point cloud topic is active
			// this is necessary to prevent delays as the depth camera starts up everytime
			void dummyCallback(pcl::PointCloud<PointT>::ConstPtr ptr) {}

			ros::NodeHandle nh;
			ros::Subscriber cloud_subscriber;
			// service that provides the subcloud
			ros::ServiceServer getSubcloudService;
	};

	// topic to subscribe to to get point cloud
	const std::string ObjectUtil::CLOUD_TOPIC = "/camera/depth_registered/points";

	/********************************************************************************************
	 ObjectUtil::ObjectUtil
	********************************************************************************************/
	ObjectUtil::ObjectUtil(std::string nodeName) : nh(nodeName) {
		cloud_subscriber = nh.subscribe(CLOUD_TOPIC, 0, &ObjectUtil::dummyCallback, this);
		getSubcloudService = nh.advertiseService("get_subcloud", &ObjectUtil::getSubcloud, this);
	}

	/********************************************************************************************
	 ObjectUtil::getSubcloud
	 service callback function for getsubcloud
	 returns the subcloud at the point specified in the request
	 if there is no subcloud at that location, this function returns false
	********************************************************************************************/
	bool ObjectUtil::getSubcloud(point_cloud_utilities::FindSubcloud::Request &req, point_cloud_utilities::FindSubcloud::Response &res){
			ROS_INFO("Finding subcloud");
			pcl::PointCloud<PointT>::Ptr subcloud(findSubcloud(req.index));
			// check that the cloud is valid
			if(subcloud.get() != NULL && subcloud->width*subcloud->height > 1){
				// convert to ros data type
				pcl::toROSMsg(*subcloud, res.subcloud);
				return true;
			}
			return false;
	}

	/********************************************************************************************
	 ObjectUtil::isObject
	 returns true if the given point cloud is an object 
	********************************************************************************************/
	bool ObjectUtil::isObject(){
		
	}

	/********************************************************************************************
	 ObjectUtil::getCloud
	 returns the next cloud from a depth topic
	********************************************************************************************/
	pcl::PointCloud<ObjectUtil::PointT>::ConstPtr ObjectUtil::getCloud(){
		return ros::topic::waitForMessage<pcl::PointCloud<PointT> >(CLOUD_TOPIC, nh, ros::Duration(30.0));
	}

	pcl::PointCloud<ObjectUtil::PointT>::Ptr ObjectUtil::findSubcloud(unsigned int pointIndex){
		pcl::NormalEstimation<PointT, pcl::Normal> ne;
		pcl::search::Search<PointT>::Ptr tree = boost::shared_ptr<pcl::search::Search<PointT> >(new pcl::search::KdTree<PointT>);
		pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);

		// get cloud
		pcl::PointCloud<PointT>::ConstPtr cloud_in = getCloud();
		if(cloud_in.get() == NULL){
			ROS_ERROR("Could not get point cloud");
			return pcl::PointCloud<PointT>::Ptr();
		}

		// remove NaN points
		pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
		std::vector<int> indices;
		pcl::removeNaNFromPointCloud(*cloud_in, *cloud, indices);
		unsigned int newIndex;
		// find new index value
		for(newIndex = 0; newIndex < indices.size(); ++newIndex){
			if(indices[newIndex] == pointIndex){
				break;
			}
		}

		// Estimate point normals
		ne.setSearchMethod(tree);
		ne.setInputCloud(cloud);
		ne.setKSearch(50);
		ne.compute(*cloud_normals);

		// setup region_growing
		pcl::RegionGrowingRGB<PointT, pcl::Normal> reg;
		reg.setMinClusterSize(50);
		reg.setMaxClusterSize(100000);
		reg.setSearchMethod(tree);
		reg.setNumberOfNeighbours(50);
		reg.setInputCloud(cloud);
		reg.setInputNormals(cloud_normals);
		reg.setSmoothnessThreshold(10.0/180.0*M_PI);
		reg.setCurvatureThreshold(1.0);

		//std::vector<pcl::PointIndices> clusters;
		pcl::PointIndices::Ptr cluster(new pcl::PointIndices);
		reg.getSegmentFromPoint(newIndex, *cluster);

		// extract cluster
		pcl::ExtractIndices<PointT> extract;
		pcl::PointCloud<PointT>::Ptr object_cloud(new pcl::PointCloud<PointT>);
		extract.setInputCloud(cloud);
		extract.setIndices(cluster);
		extract.setNegative(false);
		extract.filter(*object_cloud);

		return object_cloud;
	}

	/********************************************************************************************
	 ObjectUtil::convertToPCLPt
	 converts a geometry_msgs::PointStamped to a PointT
	********************************************************************************************/
	ObjectUtil::PointT ObjectUtil::convertToPCLPt(geometry_msgs::PointStamped inPt){
		PointT outPt;
		outPt.x = inPt.point.x;
		outPt.y = inPt.point.y;
		outPt.z = inPt.point.z;
		return outPt;
	}
}

int main(int argc, char** argv){
	ROS_INFO("Initializing cloud_utilties node");
	ros::init(argc, argv, "cloud_utilities_node");
	objectutils::ObjectUtil objutil;
	ros::spin();
}
