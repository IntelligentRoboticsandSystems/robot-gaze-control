#! /usr/bin/env python

import roslib; roslib.load_manifest('sound_play')
import rospy
import actionlib
from std_msgs.msg import String
from sound_play.msg import SoundRequest, SoundRequestAction, SoundRequestGoal
from dynamic_reconfigure.server import Server as DynamicReconfigureServer
from provide_feedback.cfg import SoundFeedbackConfig as ConfigType
from subprocess import call, check_output

import os

class SayWords:
    def __init__(self):
        rospy.init_node('sound_feedback')
        self.client = actionlib.SimpleActionClient('sound_play', SoundRequestAction)
	initialVol = check_output(["amixer -D pulse sget Master | grep -m 1 -o -E '[0-9]{1,}%'"], shell=True)
	initialVol = int(initialVol[:-2])
	rospy.set_param('/sound_feedback/sound_volume', initialVol)
	rospy.loginfo("Waiting for sound_play node")
        self.client.wait_for_server()
        self.goal = SoundRequestGoal()
	self.dynamic_reconfig_server = DynamicReconfigureServer(ConfigType, self.reconfigure)
	rospy.loginfo("Ready to run")
    
    def reconfigure(self, config, level):
	if(level <= 0):
            if(config["speech_feedback_enabled"]):
                self.wordsSubscriber = rospy.Subscriber('provide_feedback/feedback_words', String, self.say_words)
		with open(os.devnull, 'w') as devnull:
	            call(["amixer", "-D", "pulse", "sset", "Master", "unmute"], stdout=devnull)
	    elif(hasattr(self, 'wordsSubscriber')):
	        self.wordsSubscriber.unregister()
	else:
	    with open(os.devnull, 'w') as devnull:
	        call(["amixer", "-D", "pulse", "sset", "Master", str(config["sound_volume"]) + "%"])
	return config

    def say_words(self, msg):
        self.goal.sound_request.sound = SoundRequest.SAY
        self.goal.sound_request.command = SoundRequest.PLAY_ONCE
        self.goal.sound_request.arg = msg.data
        self.client.send_goal(self.goal)
        self.client.wait_for_result()

if __name__ == '__main__':
    wordsayer = SayWords()
    rospy.spin()
