/******************************************************************************
 * 
 * Author: Jeremy Webb
 * Date: August 3, 2015
 * File: visual_feedback.cpp
 * Description: A ROS node to present feedback to the user on screen
 * 
 *****************************************************************************/

#include <string>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <dynamic_reconfigure/server.h>
#include "provide_feedback/TextFeedbackConfig.h"
#include <cv_bridge/cv_bridge.h>
#include "geometry_msgs/Point32.h"
#include "std_msgs/String.h"

#include "boost/thread/mutex.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <X11/Xlib.h> // for getting screen size

class VisualFeedback{
	public:
		static const std::string IMAGE_TOPIC;
		VisualFeedback(std::string windowName = "Display");
		~VisualFeedback();
		void run();
	private:
		ros::NodeHandle nh;
		ros::NodeHandle rawnh;
		ros::Subscriber textSubscriber;
		image_transport::Subscriber imageSubscriber;
		ros::Publisher clickPublisher;
		dynamic_reconfigure::Server<provide_feedback::TextFeedbackConfig> configServer;
		std::string windowName;
		std::string instructionText;
		boost::mutex textLock;
		unsigned int pixelsX, pixelsY;
		short keyPressed;

		void displayImage(const sensor_msgs::ImageConstPtr& image);
		void getText(const std_msgs::StringConstPtr&);
		void updateConfig(provide_feedback::TextFeedbackConfig &config, uint32_t level);

	friend void mouseClickHandler(int event, int x, int y, int flags, void* userdata);

};

void mouseClickHandler(int event, int x, int y, int flags, void* userdata){
	if(event == cv::EVENT_LBUTTONDOWN){
		VisualFeedback* feedback = static_cast<VisualFeedback*>(userdata);
		geometry_msgs::Point32 msg;
		msg.x = x;
		msg.y = y;
		feedback->clickPublisher.publish(msg);
	}
}

const std::string VisualFeedback::IMAGE_TOPIC = "/camera/rgb/image_color";

VisualFeedback::VisualFeedback(std::string windowName)
	: windowName(windowName),rawnh("provide_feedback"), nh("visual_feedback") {
	image_transport::ImageTransport it(nh);
	imageSubscriber = it.subscribe(IMAGE_TOPIC, 1, &VisualFeedback::displayImage, this);
	clickPublisher = nh.advertise<geometry_msgs::Point32>("click_point", 1);
	cv::namedWindow(windowName, CV_WINDOW_NORMAL | CV_WINDOW_FREERATIO);
	cv::setWindowProperty(windowName, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
	// get window attributes for screen size
        Display* display = XOpenDisplay(NULL);
        XWindowAttributes getWinAttr;
        Window tempWin = DefaultRootWindow(display);
        XGetWindowAttributes(display, tempWin, &getWinAttr);
	pixelsX = getWinAttr.width;
	pixelsY = getWinAttr.height;
	XCloseDisplay(display);
	// initialize keyPressed
	keyPressed = -1;
	cv::setMouseCallback(windowName, mouseClickHandler, this);
	cv::startWindowThread();

	// set callback for dynamic reconfigure
	configServer.setCallback(boost::bind(&VisualFeedback::updateConfig, this, _1, _2));
}

VisualFeedback::~VisualFeedback(){
	cv::destroyWindow(windowName);
}

void VisualFeedback::updateConfig(provide_feedback::TextFeedbackConfig &config, uint32_t level){
	if(config.text_feedback_enabled){
		textSubscriber = rawnh.subscribe("feedback_words", 0, &VisualFeedback::getText, this);
	}
	else{
		textSubscriber.shutdown();
		instructionText.clear();
	}
}

void VisualFeedback::run(){
	while(nh.ok() && keyPressed != 27){
		ros::spinOnce();
		keyPressed = cv::waitKey(40) & 255;
	}
}

void VisualFeedback::displayImage(const sensor_msgs::ImageConstPtr& image){
	try{
		cv::Mat newFrame = cv_bridge::toCvShare(image)->image;
		cv::Mat resizedFrame;
		cv::resize(newFrame, resizedFrame, cv::Size(pixelsX, pixelsY));
		textLock.lock();
		cv::putText(resizedFrame, instructionText.c_str(), cv::Point(100,100), cv::FONT_HERSHEY_PLAIN, 2.5, cv::Scalar(0,0,0), 3);
		textLock.unlock();
		cv::imshow(windowName, resizedFrame);
	}
	catch(cv_bridge::Exception& e){
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", image->encoding.c_str());
	}
}

void VisualFeedback::getText(const std_msgs::StringConstPtr& message){
	textLock.lock();
	instructionText = message->data;
	textLock.unlock();
}

int main(int argc, char **argv){
	ROS_INFO("Initializing visual_feedback_node");
	ros::init(argc, argv, "visual_feedback_node");
	VisualFeedback display;
	display.run();
	return 0;
}
