/******************************************************************************
 * 
 * Author: Jeremy Webb
 * Date: July 29, 2015
 * File: PotentialField.cpp
 * Description: A class for calculating, combining potential fields
 * 
 *****************************************************************************/

#include "PotentialField.h"
#include <iostream>
#include <algorithm>

namespace potential{
	Point PotentialField::findCombinedMinimum(const std::vector<PotentialField>& fields){
		double denominatorX, denominatorY, denominatorZ;
		denominatorX = denominatorY = denominatorZ = 0.0;
		Point minimum;
		for(std::vector<PotentialField>::const_iterator it = fields.begin(); it != fields.end(); ++it){
			minimum.x += it->coefficients.weightX*it->center.x;
			minimum.y += it->coefficients.weightY*it->center.y;
			minimum.z += it->coefficients.weightZ*it->center.z;
			denominatorX += it->coefficients.weightX;
			denominatorY += it->coefficients.weightY;
			denominatorZ += it->coefficients.weightZ;
		}
		minimum.x /= denominatorX;
		minimum.y /= denominatorY;
		minimum.z /= denominatorZ;
		return minimum;
	}

	PotentialField::PotentialField(const Point& center, const Weights& coeffs, Type type)
		: center(center), coefficients(coeffs), type(type) {}
	
	double PotentialField::calcValue(const Point& location){
		switch(type){
			case PARABOLIC:
				return 1*(coefficients.weightX*pow(center.x-location.x, 2) + coefficients.weightY*pow(center.y-location.y, 2) + coefficients.weightZ*pow(center.z-location.z, 2));
			case GAUSSIAN:
				return exp(-(pow(location.x-center.x, 2)/(2*pow(coefficients.weightX,2)) + pow(location.y-center.y, 2)/(2*pow(coefficients.weightY,2)) + pow(location.z-center.z, 2)/(2*pow(coefficients.weightZ,2))));
		}
	}

	potential::Point PotentialField::getWeightedPoint(const potential::Point& location, const Weights& coordWeights){
		potential::Point result;
		double totalWeight;
		if(fabs(location.x - center.x) < THRESHOLD && fabs(location.y - center.y) < THRESHOLD && fabs(location.z - center.z) < THRESHOLD)
			return location;
		switch(type){
			case PARABOLIC:
				{
				double maxWeight = 1/(coefficients.weightX*pow(center.x-THRESHOLD, 2) + coefficients.weightY*pow(center.y-THRESHOLD, 2) + coefficients.weightZ*pow(center.z-THRESHOLD, 2));
				totalWeight = (1/(coefficients.weightX*pow(location.x-center.x, 2) + coefficients.weightY*pow(location.y-center.y, 2) + coefficients.weightZ*pow(location.z-center.z, 2)))/maxWeight;
				}
				break;
			case GAUSSIAN:
				totalWeight = calcValue(location);
				break;
		}
		result.x = location.x*(1.0-totalWeight*coordWeights.weightX) + center.x*totalWeight*coordWeights.weightX;
		result.y = location.y*(1.0-totalWeight*coordWeights.weightY) + center.y*totalWeight*coordWeights.weightY;
		result.z = location.z*(1.0-totalWeight*coordWeights.weightZ) + center.z*totalWeight*coordWeights.weightZ;
		return result;
	}
}
