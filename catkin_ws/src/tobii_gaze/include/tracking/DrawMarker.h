/************************************************************
 * 
 * Author: Jeremy Webb
 * Date: July 16, 2015
 * Description: A class to draw a marker on the screen at the specified location
 * File: DrawMarker.h
 * 
 ***********************************************************/

#pragma once

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/shape.h>
#include <X11/Xatom.h>

class DrawMarker{
	public:
		static const unsigned int DEFAULT_MARKER_SIZE = 40;
		static const float DEFAULT_MARKER_COLOR[]; // RGB values from 0-1
		DrawMarker();
		virtual ~DrawMarker();
		void draw(unsigned int x, unsigned int y, unsigned int radius = DEFAULT_MARKER_SIZE);
		void erase();
	private:
		static const unsigned short MAX_COLOR_NUMBER = 65535;
		Display* display;
		Window win;
		GC gc;
		float drawColorArray[3];
		XColor lineColor;
		unsigned int oldX, oldY;
		unsigned int markerSize;

		void eraseWithoutFlush();
};
