/**************************************************************************
 *
 * Author: Jeremy Webb
 * Date: July 14, 2015
 * File: GazeDataHolder.h
 * Description: A class to hold and process gaze data
 *
 *************************************************************************/

#pragma once

#include <math.h>
#include <numeric>

#include <boost/circular_buffer.hpp>

#include <X11/Xlib.h> // for getting screen size

namespace tobii{
	class GazeDataHolder{
		public:
			// gaze data struct
			struct GazeData{
				bool isValidData;
				// last gaze point collected without any processing
				double xRaw, yRaw;
				// data with a simple moving average applied
				// filter is applied over all points stored
				// normalized to be in the range 0 to 1
				double xFiltered, yFiltered, varianceFiltered;
				// data used for displaying the marker on screen
				// units are pixels
				double xDisplay, yDisplay, varianceDisplay;
				// data filtered to remove saccades
				// normalized to be in the range 0 to 1
				double xFixation, yFixation, varianceFixation;
				double xDwell, yDwell;
				GazeData() : varianceFiltered(-1), varianceDisplay(-1), varianceFixation(-1), isValidData(false), xDwell(NAN), yDwell(NAN) {}
				GazeData(double xRaw, double yRaw) : xRaw(xRaw), yRaw(yRaw), varianceFiltered(-1), varianceDisplay(-1), varianceFixation(-1), isValidData(true), xDwell(NAN), yDwell(NAN) {}
			};

			static const unsigned int DEFAULT_GAZE_DATA_SIZE = 10; // number of points to keep
			static const double NEW_POINT_DISTANCE_THRESHOLD = 10000; // in pixels^2
			GazeDataHolder(unsigned int gazeSize = DEFAULT_GAZE_DATA_SIZE);
			// store the new gaze point if it is within the required variance
			void storeNewPoint(double x, double y);
			// clear all stored data
			void flush();
			// return current gaze info
			GazeData getData();
			
		private:
			double xMean, yMean, variance;
			double xModifiedMean, yModifiedMean, modifiedVariance;
			double xDwell, yDwell;
			unsigned int pixelsX, pixelsY;
			unsigned int gazeDataSize; // size of the gaze data stored
			boost::circular_buffer<double> gazeDataX, originalDataX; // store the last GAZE_DATA_SIZE x coordinates
			boost::circular_buffer<double> gazeDataY, originalDataY; // store the last GAZE_DATA_SIZE y coordinates
			boost::circular_buffer<double> dwellQueueX, dwellQueueY; // store the possible dwell coordinates

	};

}
