/************************************************************
 * 
 * Author: Jeremy Webb
 * Date: July 16, 2015
 * Description: A class to draw a marker on the screen at the specified location
 * File: DrawMarker.cpp
 * 
 ***********************************************************/

#include "tracking/DrawMarker.h"

const float DrawMarker::DEFAULT_MARKER_COLOR[] = {0.1, 0.1, 0.9};

DrawMarker::DrawMarker() {
    markerSize = DEFAULT_MARKER_SIZE;
    for(int i = 0; i < 3; ++i)
    	drawColorArray[i] = DEFAULT_MARKER_COLOR[i];

    display = XOpenDisplay(NULL);

    XVisualInfo vinfo;
    XMatchVisualInfo(display, DefaultScreen(display), 32, TrueColor, &vinfo);

    XSetWindowAttributes attr;
    attr.colormap = XCreateColormap(display, DefaultRootWindow(display), vinfo.visual, AllocNone);
    attr.border_pixel = 0;
    attr.background_pixel = 0; // background pixel color
    attr.override_redirect = True; // override windows manager to get hidden window

    // get window attributes for screen size
    XWindowAttributes getWinAttr;
    Window tempWin = DefaultRootWindow(display);
    XGetWindowAttributes(display, tempWin, &getWinAttr);

    // create window
    win = XCreateWindow(display, DefaultRootWindow(display), 0, 0, getWinAttr.width, getWinAttr.height, 0, vinfo.depth, InputOutput, vinfo.visual, CWColormap | CWBorderPixel | CWBackPixel | CWOverrideRedirect, &attr);

    // allow all events to pass through (i.e. let mouse click window underneath)
    XserverRegion region = XFixesCreateRegion(display, NULL, 0);
    XFixesSetWindowShapeRegion(display, win, ShapeInput, 0, 0, region);
    XFixesDestroyRegion(display, region);

    // create graphics context
    gc = XCreateGC(display, win, 0, 0);

    lineColor.red = drawColorArray[0]*MAX_COLOR_NUMBER;
    lineColor.green = drawColorArray[1]*MAX_COLOR_NUMBER;
    lineColor.blue = drawColorArray[2]*MAX_COLOR_NUMBER;
    lineColor.flags = DoRed | DoGreen | DoBlue;
    XAllocColor(display, attr.colormap, &lineColor);

    // set stay-on-top always
    Atom stateAbove = XInternAtom(display, "_NET_WM_STATE_ABOVE", False);
    XChangeProperty(display, win, XInternAtom(display, "_NET_WM_STATE", False), XA_ATOM, 32, PropModeReplace, (unsigned char*) &stateAbove, 1);

    // show the window
    XMapWindow(display, win);

    oldX = oldY = 0;
    // set line drawing properties
    XSetLineAttributes(display, gc, 5, LineSolid, CapButt, JoinRound);
}

DrawMarker::~DrawMarker(){
    XDestroyWindow(display, win);
    XCloseDisplay(display);
}

void DrawMarker::draw(unsigned int x, unsigned int y, unsigned int radius){
	eraseWithoutFlush();
	markerSize = radius;
	XDrawArc(display, win, gc, x-(markerSize/2), y-(markerSize/2), markerSize, markerSize, 0, 360*64);
	oldX = x;
	oldY = y;
	XFlush(display);
}

void DrawMarker::eraseWithoutFlush(){
	XSetForeground(display, gc, 0);
	XDrawArc(display, win, gc, oldX-(markerSize/2), oldY-(markerSize/2), markerSize, markerSize, 0, 360*64);
	XSetForeground(display, gc, lineColor.pixel);
}

void DrawMarker::erase(){
	eraseWithoutFlush();
	XFlush(display);
}
