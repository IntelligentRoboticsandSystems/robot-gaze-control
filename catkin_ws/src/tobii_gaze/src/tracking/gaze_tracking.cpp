/*****************************************************************
 * 
 * Author: Jeremy Webb
 * Date: July 14,2015
 * File: gaze_tracking.cpp
 * Description: A ros interface to the tobii gaze sdk
 *
 ****************************************************************/

#include <exception>

#include <boost/bind.hpp>
#include <boost/thread/mutex.hpp>

#include <ros/ros.h>
#include "tobii_gaze/GazeData.h"
#include "tobii_gaze/MarkerDisplay.h"
#include "std_srvs/Empty.h"
#include "std_srvs/SetBool.h"

#include "tracking/GazeTracking.h"
#include "tracking/DrawMarker.h"

namespace tobii{
	class Tracking{
		public:
			Tracking();
			bool start(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
			bool stop(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res);
			bool setDisplayMarker(tobii_gaze::MarkerDisplay::Request &req, tobii_gaze::MarkerDisplay::Response &res);
		private:
			ros::NodeHandle nh;
			ros::Publisher gazePublisher;
			ros::Publisher dwellPublisher;
			ros::ServiceServer startService;
			ros::ServiceServer stopService;
			ros::ServiceServer markerService;
			GazeTracking tracker;
			DrawMarker drawer;
			bool draw, drawFreeze;
			boost::mutex drawerLock;
		
			void gazeCallback(const GazeDataHolder::GazeData& data);
	};

	Tracking::Tracking() : nh(ros::NodeHandle("tobii")), draw(true) {
		gazePublisher = nh.advertise<tobii_gaze::GazeData>("gaze", 1);
		dwellPublisher = nh.advertise<tobii_gaze::GazeData>("dwell", 0);
		startService = nh.advertiseService("start_tracking", &Tracking::start, this);
		stopService = nh.advertiseService("stop_tracking", &Tracking::stop, this);
		markerService = nh.advertiseService("marker_display", &Tracking::setDisplayMarker, this);
	}

	bool Tracking::start(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res){
		tracker.startTracking(boost::bind(&Tracking::gazeCallback, this, _1));
		return true;
	}

	bool Tracking::stop(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res){
		tracker.stopTracking();
		if(!req.data){
			drawerLock.lock();
			drawer.erase();
			drawerLock.unlock();
		}
		return true;
	}

	bool Tracking::setDisplayMarker(tobii_gaze::MarkerDisplay::Request &req, tobii_gaze::MarkerDisplay::Response &res){
		draw = req.on;
		drawFreeze = req.freeze;
		if(!draw){
			drawerLock.lock();
			drawer.erase();
			drawerLock.unlock();
		}
		return true;
	}

	void Tracking::gazeCallback(const GazeDataHolder::GazeData& data){
		drawerLock.lock();
		if(draw && data.isValidData && !drawFreeze)
			drawer.draw(data.xDisplay, data.yDisplay);
		else if(!drawFreeze)
			drawer.erase();
		drawerLock.unlock();
		tobii_gaze::GazeData message;
		message.header.stamp = ros::Time::now();
		message.isTracked = data.isValidData;
		message.x = data.xFixation;
		message.y = data.yFixation;
		message.variance = data.varianceFixation;
		gazePublisher.publish(message);
		if(!isnan(data.xDwell) && !isnan(data.yDwell)){
			tobii_gaze::GazeData dwellMessage;
			dwellMessage.header.stamp = ros::Time::now();
			dwellMessage.isTracked = true;
			dwellMessage.x = data.xDwell;
			dwellMessage.y = data.yDwell;
			dwellPublisher.publish(dwellMessage);
		}
	}
}

int main(int argc, char **argv){
	ROS_INFO("Initializing gaze_tracking_node");
	ros::init(argc, argv, "gaze_tracking_node");
	tobii::Tracking device;
	ros::spin();
	return 0;
}
