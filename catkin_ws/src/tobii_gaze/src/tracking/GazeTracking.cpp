/**************************************************************************
 *
 * Author: Jeremy Webb
 * Date: July 13, 2015
 * File: GazeTracking.cpp
 * Description: A class to interface with the tobii gaze sdk
 *
 *************************************************************************/

#include "tracking/GazeTracking.h"

namespace tobii{
	GazeTracking::GazeTracking(unsigned int gazeSize) {
		isTracking = false;

		// if there is a tracker connected, get its url
		tobiigaze_get_connected_eye_tracker(url, URL_SIZE, &error_code);
		handleErrorMessage(std::string("No eye tracker found"));

		// Create an eye tracker instance.
		eye_tracker = tobiigaze_create(url, &error_code);
		handleErrorMessage("tobiigaze_create");

		// Enable diagnostic error reporting 
		tobiigaze_register_error_callback(eye_tracker, handleError, NULL);
		//tobiigaze_set_logging("minimaltracker.log", TOBIIGAZE_LOG_LEVEL_INFO, NULL);
	
		// start the event loop. This must be done before connecting.
		tobiigaze_run_event_loop_on_internal_thread(eye_tracker, 0, 0);

	}

	GazeTracking::~GazeTracking(){
		if(isTracking){
			stopTracking();
		}
		tobiigaze_break_event_loop(eye_tracker);
		tobiigaze_destroy(eye_tracker);
	}

	// register a new gaze data callback
	// will replace old one
	void GazeTracking::registerCallback(gazeDataCallback gaze_callback){
		gazeCallback = gaze_callback;
	}

	void GazeTracking::startTracking(gazeDataCallback gaze_callback){
		if(!isTracking){
			// register the callback
			registerCallback(gaze_callback);

			// connect to the tracker.
			tobiigaze_connect(eye_tracker, &error_code);
			handleErrorMessage("Could not connect to tracker");

			// Start tracking
			tobiigaze_start_tracking_async(eye_tracker, &start_tracking_callback, &on_gaze_data, this);
		}
	}

	// stop tracking
	void GazeTracking::stopTracking(){
		if(isTracking){
			tobiigaze_stop_tracking(eye_tracker, &error_code);
			handleErrorMessage("Could not stop tracking");

			// disconnect and clean up
			tobiigaze_disconnect(eye_tracker);
			isTracking = false;
		}
		dataContainer.flush();
	}

	// query if the device is tracking
	bool GazeTracking::isCurrentlyTracking(){
		return isTracking;
	}

	// error handling member function
	void GazeTracking::handleErrorMessage(std::string message)
	{
		handleError(error_code, &message);
	}

	// callback called after tracking has started
	void start_tracking_callback(tobiigaze_error_code error_code, void *user_data){
		GazeTracking* tracker = static_cast<GazeTracking*>(user_data);
		tracker->handleErrorMessage("Could not start tracking");
		tracker->isTracking = true;
	}

	// Error callback function
	void handleError(tobiigaze_error_code error_code, void *user_data)
	{
		std::string* message;
		std::string tobii_message;
		std::string messagePrefix("Error: ");
		if(error_code){
			if(user_data != NULL)
				message = static_cast<std::string*>(user_data);
			tobii_message.assign(tobiigaze_get_error_message(error_code));
			throw GazeTracking::GazeTrackingException(messagePrefix + *message + ";\nTobii Error: " + tobii_message);
		}
	}

	// Prints gaze information, or "-" if gaze position could not be determined.
	void on_gaze_data(const tobiigaze_gaze_data* gazedata, const tobiigaze_gaze_data_extensions* extensions, void *user_data)
	{
		//printf("%20.3f ", gazedata->timestamp/ 1e6); // in seconds
		tobiigaze_point_2d newData;
		bool isValid = false;
		if(gazedata->tracking_status == TOBIIGAZE_TRACKING_STATUS_BOTH_EYES_TRACKED){
			newData.x = (gazedata->left.gaze_point_on_display_normalized.x + gazedata->right.gaze_point_on_display_normalized.x)/2.0;
			newData.y = (gazedata->left.gaze_point_on_display_normalized.y + gazedata->right.gaze_point_on_display_normalized.y)/2.0;
			isValid = true;
		}
		else if(gazedata->tracking_status == TOBIIGAZE_TRACKING_STATUS_ONLY_LEFT_EYE_TRACKED ||
				gazedata->tracking_status == TOBIIGAZE_TRACKING_STATUS_ONE_EYE_TRACKED_PROBABLY_LEFT){
			newData = gazedata->left.gaze_point_on_display_normalized;
			isValid = true;
		}
		else if(gazedata->tracking_status == TOBIIGAZE_TRACKING_STATUS_ONLY_RIGHT_EYE_TRACKED ||
				gazedata->tracking_status == TOBIIGAZE_TRACKING_STATUS_ONE_EYE_TRACKED_PROBABLY_RIGHT){
			newData = gazedata->right.gaze_point_on_display_normalized;
			isValid = true;
		}

		GazeTracking* tracker = static_cast<GazeTracking*>(user_data);
		GazeDataHolder::GazeData result;
		if(isValid){
			tracker->dataContainer.storeNewPoint(newData.x, newData.y);
			result = tracker->dataContainer.getData();
			result.isValidData = true;
		}
		// notify callback of new data
		tracker->gazeCallback(result); 
	}

}
