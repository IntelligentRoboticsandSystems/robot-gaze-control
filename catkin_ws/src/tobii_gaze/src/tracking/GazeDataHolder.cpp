/**************************************************************************
 *
 * Author: Jeremy Webb
 * Date: July 14, 2015
 * File: GazeDataHolder.cpp
 * Description: A class to hold and process gaze data
 *
 *************************************************************************/

#include "tracking/GazeDataHolder.h"

namespace tobii{
	GazeDataHolder::GazeDataHolder(unsigned int gazeSize) : gazeDataX(gazeSize), gazeDataY(gazeSize), originalDataX(gazeSize), originalDataY(gazeSize), dwellQueueX(270), dwellQueueY(270), xModifiedMean(0), yModifiedMean(0), modifiedVariance(-1), xMean(0.0), yMean(0.0), variance(-1) {
	        // get window attributes for screen size
	        Display* display = XOpenDisplay(NULL);
	        XWindowAttributes getWinAttr;
	        Window tempWin = DefaultRootWindow(display);
	        XGetWindowAttributes(display, tempWin, &getWinAttr);
		pixelsX = getWinAttr.width;
		pixelsY = getWinAttr.height;
    		XCloseDisplay(display);
	}

	// store the new gaze point if it is within the required variance
	void GazeDataHolder::storeNewPoint(double x, double y){
		// add the point to the original data store
		originalDataX.push_back(x);
		originalDataY.push_back(y);
		// calculate the distance of the current point from the previous mean in pixels
		double totalDistance = pow((xModifiedMean - x)*pixelsX, 2) + pow((yModifiedMean - y)*pixelsY, 2);
		if(totalDistance < NEW_POINT_DISTANCE_THRESHOLD){
			// if the point falls within the current fixation add it to queue
			gazeDataX.push_back(x);
			gazeDataY.push_back(y);
		}
		else{
			// if the size of the queue is greater than 0 remove the oldest
			// 	point since it doesn't fall within the current fixation
			if(gazeDataX.size() > 0){
				gazeDataX.pop_front();
				gazeDataY.pop_front();
			}
			if(gazeDataX.size() <= 1){
			// there is only one point,  add latest point
				gazeDataX.push_back(x);
				gazeDataY.push_back(y);
			}
		}
		// calculate new means
		double xSum = std::accumulate(gazeDataX.begin(), gazeDataX.end(), 0.0);
		double ySum = std::accumulate(gazeDataY.begin(), gazeDataY.end(), 0.0);
		xModifiedMean = xSum/gazeDataX.size();
		yModifiedMean = ySum/gazeDataY.size();
		xMean = std::accumulate(originalDataX.begin(), originalDataX.end(), 0.0)/originalDataX.size();
		yMean = std::accumulate(originalDataY.begin(), originalDataY.end(), 0.0)/originalDataX.size();
		// calculate variance
		double var;
		for(int i = 0; i < gazeDataX.size(); ++i){
			// x variance + y variance + 2*covariance
			var += pow((gazeDataX[i]-xModifiedMean)*pixelsX/pixelsY, 2) + pow((gazeDataY[i]-yModifiedMean), 2) + 2*(gazeDataX[i]-xModifiedMean)*(gazeDataY[i]-yModifiedMean);
		}
		modifiedVariance =  var;

		for(int i = 0; i < originalDataX.size(); ++i){
			// x variance + y variance + 2*covariance
			var += pow((originalDataX[i]-xMean)*pixelsX/pixelsY, 2) + pow((originalDataY[i]-yMean), 2) + 2*(originalDataX[i]-xMean)*(originalDataY[i]-yMean);
		}
		variance = var;

		// calculate dwell event
		dwellQueueX.push_back(xModifiedMean);
		dwellQueueY.push_back(yModifiedMean);
		if(dwellQueueX.size() == 270){
			double dwellSumX = std::accumulate(dwellQueueX.begin(), dwellQueueX.end(), 0.0);
			
			double dwellSumY = std::accumulate(dwellQueueY.begin(), dwellQueueY.end(), 0.0);
			double dwellMeanX = dwellSumX/dwellQueueX.size();
			double dwellMeanY = dwellSumY/dwellQueueY.size();
			double modifiedDwellX, modifiedDwellY;
			modifiedDwellX = modifiedDwellY = 0.0;
			unsigned int numInsideCircle = 0;
			for(int i = 0; i < dwellQueueX.size(); i++){
				if(pow((dwellQueueX[i]-dwellMeanX)*pixelsX,2)+pow((dwellQueueY[i]-dwellMeanY)*pixelsY,2) < 40000){
					++numInsideCircle;
					modifiedDwellX += dwellQueueX[i];
					modifiedDwellY += dwellQueueY[i];
				}
			}
			if(numInsideCircle > 250){
				// dwell event occured
				xDwell = modifiedDwellX/numInsideCircle;
				yDwell = modifiedDwellY/numInsideCircle;
				// empty dwell buffers
				dwellQueueX.clear();
				dwellQueueY.clear();
			}
			else{
				// no dwell event, throw away beginning ten data points
				xDwell = NAN;
				yDwell = NAN;
				for(int i = 0; i < 10; ++i){
					dwellQueueX.pop_front();
					dwellQueueY.pop_front();
				}
			}
		}
		else{
			xDwell = NAN;
			yDwell = NAN;
		}
	}

	// clear all stored data
	void GazeDataHolder::flush(){
		xModifiedMean = yModifiedMean = 0;
		modifiedVariance = -1;
		gazeDataX.clear();
		gazeDataY.clear();
		originalDataX.clear();
		originalDataY.clear();
	}

	// return current gaze info
	GazeDataHolder::GazeData GazeDataHolder::getData(){
		GazeData data(originalDataX.back(), originalDataY.back());
		data.xFiltered = xMean;
		data.yFiltered = yMean;
		data.varianceFiltered = variance;
		data.xDisplay = xMean*pixelsX;
		data.yDisplay = yMean*pixelsY;
		data.varianceDisplay = variance*pixelsX;
		data.xFixation = xModifiedMean;
		data.yFixation = yModifiedMean;
		data.varianceFixation = modifiedVariance;
		data.xDwell = xDwell;
		data.yDwell = yDwell;
		return data;
	}
}
