/*
 * Copyright 2014 Tobii Technology AB. All rights reserved.
 */

#include <wx/wx.h>
#include <wx/event.h>
#include <wx/frame.h>
#include "calibration/CalibrationViewModel.h"
#include "calibration/TestingViewModel.h"
#include "calibration/CalibrationWindow.h"
#include "calibration/ICalibrationViewModel.h"

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <tobii_gaze/CalibrationAction.h>

#include <boost/thread/condition_variable.hpp>

/**
 * Application class. Provides the application entry point.
 */
class CalibrationApp : public wxApp
{
public:
    bool OnInit();
    int OnRun();
    int OnExit();

    void rosSpin(wxIdleEvent &event);
    void startNewSession(const tobii_gaze::CalibrationGoalConstPtr& goal);
    void cancelSession();
    void openWindows(wxCommandEvent& event);

    DECLARE_EVENT_TABLE();

private:
    ICalibrationViewModel* m_viewModel;
    CalibrationWindow* frame; 
    ros::NodeHandlePtr nhPtr;
    actionlib::SimpleActionServer<tobii_gaze::CalibrationAction>* calibrationServer;
    char** argvForRos;

    boost::condition_variable cond;
    boost::mutex mut;
    RosActionState server_state;
};

const int START_ID = 10000;

IMPLEMENT_APP(CalibrationApp);

BEGIN_EVENT_TABLE(CalibrationApp, wxApp)
    EVT_IDLE(CalibrationApp::rosSpin)
    EVT_COMMAND(START_ID, wxEVT_COMMAND_TEXT_UPDATED, CalibrationApp::openWindows)
END_EVENT_TABLE()


bool CalibrationApp::OnInit()
{
    // Initialize ros
    // create a copy of argv with regular char's
    argvForRos = new char*[argc];
    for(int i = 0; i < argc; ++i){
	argvForRos[i] = strdup(wxString(argv[i]).mb_str());
    }
    ros::init(argc, argvForRos, "calibration_node");
    nhPtr.reset(new ros::NodeHandle);
    calibrationServer = new actionlib::SimpleActionServer<tobii_gaze::CalibrationAction>(*nhPtr, "Calibration", boost::bind(&CalibrationApp::startNewSession, this, _1), false);
    calibrationServer->registerPreemptCallback(boost::bind(&CalibrationApp::cancelSession, this));
 
    frame = NULL;
    m_viewModel = NULL;
    wxFrame* topWindow = new wxFrame();
    SetTopWindow(topWindow);
    SetExitOnFrameDelete(false);
    return true;
}

int CalibrationApp::OnRun(){
    calibrationServer->start();
    return wxApp::OnRun();
}

int CalibrationApp::OnExit()
{
    if(m_viewModel != NULL)
        delete m_viewModel;
    delete calibrationServer;
    for(int i = 0; i < argc; ++i){
	free(argvForRos[i]);
    }

    delete[] argvForRos;

    return wxApp::OnExit();
}

void CalibrationApp::openWindows(wxCommandEvent& event){
	// delete old viewModel
	if(m_viewModel){
	    delete m_viewModel;
	    m_viewModel = NULL;
	}

        // Create the view model.
        // We have the choice of a CalibrationViewModel, which does actual work, or 
        // a TestingViewModel, which can be used to test that the GUI does what it 
        // is supposed to.
        m_viewModel = new CalibrationViewModel();
        //m_viewModel = new TestingViewModel();
    
        frame = new CalibrationWindow(*m_viewModel, cond, mut, server_state);
}

void CalibrationApp::startNewSession(const tobii_gaze::CalibrationGoalConstPtr& goal){
	server_state = IN_PROGRESS;
	boost::unique_lock<boost::mutex> lock(mut);
	wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED, START_ID);
	GetTopWindow()->GetEventHandler()->AddPendingEvent(event);
	while(server_state == IN_PROGRESS){cond.wait(lock);}
	if(server_state == DONE)
		calibrationServer->setSucceeded();
	else if(server_state == ERROR || server_state == CANCELLED)
		calibrationServer->setAborted();
	else
		calibrationServer->setPreempted();
	// delete old viewModel
	if(m_viewModel){
	    delete m_viewModel;
	    m_viewModel = NULL;
	}
}

void CalibrationApp::cancelSession(){
	ROS_INFO("Cancelling calibration");
	boost::lock_guard<boost::mutex> lock(mut);
	server_state = PREEMPTED;
	frame->Close();
	cond.notify_one();
}

void CalibrationApp::rosSpin(wxIdleEvent &event){
    if(!nhPtr->ok()){
	ExitMainLoop();
	return;
    }
	
    ros::spinOnce();
    if(IsMainLoopRunning())
	event.RequestMore();
}
