# Robot Gaze Control

This project allows a Mico robot arm to be controlled with the users gaze only. It employs the dwell time method to confirm the target object. Instructions are printed or spoken to the user. The main purpose of this project is to provide a cool demo.

## Getting Started

The following instructions will help set up your system for the demo provided by this project.

### Prerequisites

In order to perform this demo, you will need a Kinova Mico arm, and a Tobii eye tracker which is already setup. This repository assumes you have ros-indigo already installed. In addition you will need the sound_play package for ROS Indigo (``sudo apt-get install ros-indigo-sound-play``), and the Tobii SDK.

### Installing

1. Clone this repository.
3. Compile ros packages from the catkin_ws folder (``catkin_make``).

## Operating instructions:

Follow the instructions in the Documentation folder.

### Explanation of Parameters
The text_feedback controls whether or not instructions are printed to the top of the screen. Untick this box to remove the instructions.
The speech_feedback controls whether or not instructions are spoken to the user.
The sound_volume controls the system volume.


## Author

Jeremy Webb